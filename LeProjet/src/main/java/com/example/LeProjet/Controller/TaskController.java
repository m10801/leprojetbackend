package com.example.LeProjet.Controller;

import com.example.LeProjet.Model.Task;
import com.example.LeProjet.Service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
public class TaskController {
    @Autowired
    private TaskService taskService;

    @GetMapping("/test")
    public String getTest() {
        return "Test";
    }


    @GetMapping("/task/{idTask}")
    public ResponseEntity getTasksById(@PathVariable int idTask) {
        Optional<Task> task = taskService.getTaskById(idTask);
        return new ResponseEntity<>(task,HttpStatus.OK);
    }

    @PostMapping("/task/create")
    public ResponseEntity createTask(@RequestBody Task task) {
        Task taskCreate = taskService.createTask(task);
        return new ResponseEntity<>(taskCreate,HttpStatus.OK);
    }

    @PutMapping("/task/{id}")
    public ResponseEntity updateTask(@PathVariable int id , @RequestBody Task task) {
        try
        {
            taskService.update(task, id);
        }
        catch (Exception e)
        {
            return new  ResponseEntity(HttpStatus.FORBIDDEN);
        }
        return new ResponseEntity<>(task,HttpStatus.OK);
    }

    @DeleteMapping("/task/{id}")
    public ResponseEntity deleteTask(@PathVariable int id) {
        try{
            taskService.deleteTask(id);
        }catch ( Exception e){
            return new  ResponseEntity(HttpStatus.FORBIDDEN);
        }
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/tasks")
    public List<Task> getTasksAll() {
        return taskService.getTaskAll();

    }
}
