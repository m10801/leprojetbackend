package com.example.LeProjet.Service;

import com.example.LeProjet.Repository.ITaskRepository;
import com.example.LeProjet.Model.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TaskService {
    @Autowired
    private ITaskRepository taskRepository;


    public Optional<Task> getTaskById(Integer idTask) {
        Optional<Task> task = taskRepository.findById(idTask);
        return task;
    }

    public Task createTask(Task task) {
        taskRepository.save(task);
        return task;
    }

    public void deleteTask( int id) {
        taskRepository.deleteById(id);
    }

    public void update( Task task, int id) {

        Optional<Task> taskOptional = getTaskById(id);
        Task taskToUpdated = taskOptional.get(); // taskOptional.get() renvoie une tache
        if(taskOptional.isPresent())
        {
            taskToUpdated.title = task.title;
            taskToUpdated.description = task.description;
            taskRepository.save(taskToUpdated);
        }
    }

    public List<Task> getTaskAll() {
        List<Task> task = (List<Task>) taskRepository.findAll();
        return task;
    }
}
