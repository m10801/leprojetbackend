package com.example.LeProjet.Repository;

import com.example.LeProjet.Model.Task;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ITaskRepository extends CrudRepository<Task, Integer> {

}
